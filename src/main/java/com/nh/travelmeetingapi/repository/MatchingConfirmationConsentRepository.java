package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.entity.MatchingConfirmationConsent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface MatchingConfirmationConsentRepository extends JpaRepository<MatchingConfirmationConsent, Long> {

    List<MatchingConfirmationConsent> findAllByPackageIdAndApplicant_Member_IsMen(PackageId packageIds, Boolean isMen);



}
