package com.nh.travelmeetingapi.repository;


import com.nh.travelmeetingapi.entity.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentsRepository extends JpaRepository<Comments, Long> {

}
