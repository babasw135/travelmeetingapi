package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.QuestionAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionAnswerRepository extends JpaRepository<QuestionAnswer, Long> {
}
