package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.MyPage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MypageRepository extends JpaRepository<MyPage, Long> {
}
