package com.nh.travelmeetingapi.repository;


import com.nh.travelmeetingapi.entity.PackageId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PackageRepository extends JpaRepository<PackageId, Long> {
}
