package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByAccount(String account);
    Optional<Member> findByAccountAndPassword(String account, String password); //로그인 하기
}
