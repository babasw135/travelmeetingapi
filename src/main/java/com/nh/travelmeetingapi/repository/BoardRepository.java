package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.Board;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board, Long> {
}
