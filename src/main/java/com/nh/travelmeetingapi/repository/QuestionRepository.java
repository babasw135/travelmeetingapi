package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;


public interface QuestionRepository extends JpaRepository<Question, Long> {
}
