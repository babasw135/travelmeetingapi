package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApplicantRepository extends JpaRepository<Applicant, Long> {

    List<Applicant> findAllByIsConfirmation(Boolean isConfirmation);
}
