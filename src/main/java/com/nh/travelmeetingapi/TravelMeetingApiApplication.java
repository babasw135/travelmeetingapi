package com.nh.travelmeetingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelMeetingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelMeetingApiApplication.class, args);
	}

}
