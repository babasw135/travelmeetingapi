package com.nh.travelmeetingapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Comments { // 댓글
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "boardId", nullable = false)
    private Board board;

    @Column(nullable = false)
    private String comments; // 댓글작성

    @Column(nullable = false)
    private LocalDateTime writingTime; // 작성시간
}
