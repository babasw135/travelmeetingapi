package com.nh.travelmeetingapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class PackageId { //패키지 등록
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String PackageName;

    @Column(nullable = false)
    private LocalDate matchingStartDay;

    @Column(nullable = false)
    private LocalDate matchingEndDay;

    @Column(nullable = false)
    private String place; //장소
}
