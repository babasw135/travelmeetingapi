package com.nh.travelmeetingapi.model.applicant;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.PackageId;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ApplicantItem {
    private Long id;
    private Long member;
    private String memberName;
    private String isConfirmation;
    private String applicantDay;
}
