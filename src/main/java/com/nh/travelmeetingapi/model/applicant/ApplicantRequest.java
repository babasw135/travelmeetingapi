package com.nh.travelmeetingapi.model.applicant;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ApplicantRequest {
    private LocalDateTime applicantDay;
}
