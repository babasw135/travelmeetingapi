package com.nh.travelmeetingapi.model.Question;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionChangeRequest {
    private String title;
    private String content;
}
