package com.nh.travelmeetingapi.model.pay;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayResponse { // 하나의 구매내역만 불러오기
    private Long id;
    private Long member;
    private Long matchingConfirmationConsent;
    private String payCode;
    private String datePay;
    private String payBy;
    private String isCancel;
}
