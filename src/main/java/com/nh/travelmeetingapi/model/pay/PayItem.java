package com.nh.travelmeetingapi.model.pay;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayItem { // 구매내역 전체 불러내기
    private Long id;
    private Long member;
    private Long matchingConfirmationConsent;
    private String payCode;
    private String datePay;
    private String payBy;
    private String isCancel;
}
