package com.nh.travelmeetingapi.model.board;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BoardItem {
    private Long id;
    private String memberName;
    private String img;
    private LocalDateTime dateWrite;
    private String title;
    private String content;
}
