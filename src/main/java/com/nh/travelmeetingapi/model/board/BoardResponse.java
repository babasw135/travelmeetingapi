package com.nh.travelmeetingapi.model.board;

import com.nh.travelmeetingapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class BoardResponse {
    private Long id;
    private String img;
    private String memberName;
    private LocalDateTime dateWrite;
    private String title;
    private String content;
}
