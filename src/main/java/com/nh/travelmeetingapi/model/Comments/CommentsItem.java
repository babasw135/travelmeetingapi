package com.nh.travelmeetingapi.model.Comments;

import com.nh.travelmeetingapi.entity.Board;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentsItem {
    private Long id;
    private String comments;
    private Long board;
    private String writingTime;
}
