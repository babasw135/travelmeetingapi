package com.nh.travelmeetingapi.model.Comments;

import com.nh.travelmeetingapi.entity.Board;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentsResponse {
    private Long id;
    private Long board;
    private String comments;
    private String writingTime;
}
