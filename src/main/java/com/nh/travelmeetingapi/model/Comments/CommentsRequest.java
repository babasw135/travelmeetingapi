package com.nh.travelmeetingapi.model.Comments;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentsRequest {
    private String comments;
    private LocalDateTime writingTime;
}
