package com.nh.travelmeetingapi.model.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {
    private String msg;
    private Integer code;
}
