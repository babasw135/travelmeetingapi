package com.nh.travelmeetingapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberDupCheckResponse {
    private Boolean isNew;

}
