package com.nh.travelmeetingapi.model.member.Login;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class LoginResponse {
    private Long memberId;
    private String memberName;
}
