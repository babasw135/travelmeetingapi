package com.nh.travelmeetingapi.model.member;


import com.nh.travelmeetingapi.enums.member.Gender;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {
    private String name;

    private String account;

    private String password;

    private String passwordRe;

    private LocalDate birthday;

    private String phoneNumber;

    private Boolean isMen;
//    private Gender gender;

    private String eMail;

    private String address;

}

