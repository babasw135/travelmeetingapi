package com.nh.travelmeetingapi.model.member;


import com.nh.travelmeetingapi.enums.member.Gender;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MemberItem {
    private Long id;

    private String name;

    private Boolean isNormal;

    private String account;

    private String password;

    private String birthday;

    private String phoneNumber;

    private String dateJoin;

    private String isMen;
//    private String gender;

    private String eMail;

    private String address;

    private String grade;

    private String reason;

    private String state;
}
