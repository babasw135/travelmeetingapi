package com.nh.travelmeetingapi.model.QuestionAnswer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionAnswerCreateRequest {
    private String questionAnswer;
}
