package com.nh.travelmeetingapi.model.Package;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PackageRequest {

    private String PackageName;
    private LocalDate matchingStartDay;
    private LocalDate matchingEndDay;
    private String place;
}
