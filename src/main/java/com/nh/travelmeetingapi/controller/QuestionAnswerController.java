package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Question;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerChangeRequest;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerCreateRequest;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerItem;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerResponse;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.service.QuestionAnswerService;
import com.nh.travelmeetingapi.service.QuestionService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/question-answer")
public class QuestionAnswerController {
    private final QuestionService questionService;
    private final QuestionAnswerService questionAnswerService;

    @PostMapping("/new/{questionId}")
    @Operation(summary = "1대1 답장" )
    public CommonResult setQuestionAnswer(@PathVariable long questionId, @RequestBody QuestionAnswerCreateRequest request){
        Question question = questionService.getData(questionId);
        questionAnswerService.setQuestionAnswer(question, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "1대1 문의 답장 불러오기" )
    public ListResult<QuestionAnswerItem> getQuestionAnswers(){
        return ResponseService.getListResult(questionAnswerService.getQuestionAnswers());
    }

    @GetMapping("/detail/{questionAnswerId}")
    @Operation(summary = "1대1 특정 답변 불러오기" )
    public SingleResult<QuestionAnswerResponse> getQuestionAnswer(@PathVariable long questionAnswerId){
        return ResponseService.getSingleResult(questionAnswerService.getQuestionAnswer(questionAnswerId));
    }

    @PutMapping("/change/{questionAnswerId}")
    @Operation(summary = "1대1 문의 답변 수정" )
    public CommonResult putQuestionAnswer(@PathVariable long questionAnswerId, @RequestBody QuestionAnswerChangeRequest request){
        questionAnswerService.putQuestionAnswer(questionAnswerId, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delete/{questionAnswerId}")
    @Operation(summary = "1대1 문의 답변 삭제" )
    public CommonResult delQuestionAnswer(@PathVariable long questionAnswerId){
        questionAnswerService.delQuestionAnswer(questionAnswerId);
        return ResponseService.getSuccessResult();
    }
}
