package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Board;
import com.nh.travelmeetingapi.model.Comments.CommentsItem;
import com.nh.travelmeetingapi.model.Comments.CommentsRequest;
import com.nh.travelmeetingapi.model.Comments.CommentsResponse;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.repository.BoardRepository;
import com.nh.travelmeetingapi.service.BoardService;
import com.nh.travelmeetingapi.service.CommentsService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("/v1/comments")
public class CommentsController {
    private final CommentsService commentsService;
    private final BoardService boardService;
    @PostMapping("/new/{boardId}")
    @Operation(summary = "댓글 등록")
    public CommonResult setComments(@PathVariable long boardId,@RequestBody CommentsRequest request){
        Board board = boardService.getData(boardId);
        commentsService.setComments(board, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "댓글 불러오기")
    public ListResult<CommentsItem> getComments(){
        return ResponseService.getListResult(commentsService.getComments());
    }

    @GetMapping("/detail/{commentId}")
    @Operation(summary = "특정 댓글 불러오기")
    public SingleResult<CommentsResponse> getComment(@PathVariable long commentId){
        return ResponseService.getSingleResult(commentsService.getComment(commentId));
    }

    @PutMapping("/commentId/{Id}")
    @Operation(summary = "댓글 수정")
    public CommonResult putComment(@PathVariable long Id, @RequestBody CommentsRequest request){
        commentsService.putComment(Id, request);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delcomment/{commentId}")
    @Operation(summary = "댓글 수정")
    public CommonResult delComment(@PathVariable long commentId){
        commentsService.delComment(commentId);
        return ResponseService.getSuccessResult();
    }
}
