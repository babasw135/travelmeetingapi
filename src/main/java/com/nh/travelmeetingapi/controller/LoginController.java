package com.nh.travelmeetingapi.controller;


import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.model.member.Login.LoginRequest;
import com.nh.travelmeetingapi.model.member.Login.LoginResponse;
import com.nh.travelmeetingapi.service.LoginService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

    @PostMapping("/new")
    @Operation(summary = "로그인")
    public SingleResult<LoginResponse> doLogin(@RequestBody LoginRequest request){
        return loginService.doLogin(request);
    }
}
