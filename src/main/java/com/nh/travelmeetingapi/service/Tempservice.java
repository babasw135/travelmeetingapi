package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.enums.member.Gender;
import com.nh.travelmeetingapi.enums.member.Grade;
import com.nh.travelmeetingapi.enums.member.State;
import com.nh.travelmeetingapi.ilb.CommonFile;
import com.nh.travelmeetingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayDeque;

@Service
@RequiredArgsConstructor

public class Tempservice { // 대량 등록
    private final MemberRepository memberRepository;

    public void setRentCar(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.mltipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));//버퍼에 파일을 올려두고 버퍼안에 파일을 읽는다.

        // 버퍼에서 넘어온 string한줄을 임시로 담아줄 변수가 필요하다.
        String line = "";
        // 줄 번호 수동체크 하기 위해 int로 줄 번호 1식 증가해서 기록 해 둘 변수가 필요하다.
        int index = 0;

        while ((line = bufferedReader.readLine()) != null){
            if (index > 0 ){
              String[] cols = line.split(",");
              if (cols.length == 13){// 이 배열의 길이를 정한다. 그래야 null값이 있을 떄 에러가 뜨지 않는다.
                  System.out.println(cols[0]);
                  System.out.println(cols[1]);
                  System.out.println(cols[2]);
                  System.out.println(cols[3]);
                  System.out.println(cols[4]);
                  System.out.println(cols[5]);
                  System.out.println(cols[6]);
                  System.out.println(cols[7]);
                  System.out.println(cols[8]);
                  System.out.println(cols[9]);
                  System.out.println(cols[10]);
                  System.out.println(cols[11]);
                  System.out.println(cols[12]);
                  System.out.println(cols[13]);


                  // 이제 db에 넣자.
                  Member addData = new Member();
                  addData.setName(cols[0]);
                  addData.setIsNormal(Boolean.parseBoolean(cols[1]));
                  addData.setAccount(cols[2]);
                  addData.setPassword(cols[3]);
                  addData.setBirthday(LocalDate.parse(cols[4]));
                  addData.setPhoneNumber(cols[5]);
                  addData.setDateJoin(LocalDate.parse(cols[6]));
                  addData.setIsMen(Boolean.parseBoolean(cols[7]));
                  addData.setEMail(cols[8]);
                  addData.setAddress(cols[9]);
                  addData.setGrade(Grade.valueOf(cols[10]));
                  addData.setReason(cols[11]);
                  addData.setState(State.valueOf(cols[12]));
                  addData.setEtcMemo(cols[13]);

                  memberRepository.save(addData);

              }

            }
            index++;
        }

        bufferedReader.close(); // 다 읽었으면 닫아 주어야 한다.

    }
}
