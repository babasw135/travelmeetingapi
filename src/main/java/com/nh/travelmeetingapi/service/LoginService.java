package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.model.member.Login.LoginRequest;
import com.nh.travelmeetingapi.model.member.Login.LoginResponse;
import com.nh.travelmeetingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;

    public SingleResult<LoginResponse> doLogin(LoginRequest request){
        // 아이디와 비밀번호가 일치하는 데이터 가져온다.
        Optional<Member> member = memberRepository.findByAccountAndPassword(
                request.getAccount(),
                request.getPassword()
        );
        //회원이 데이터가 있을 수 도 없을 수도 있다.
        //만약에 회원데이터가 있으면... LoginResponse 채워서 준다.
        //근데 없으면? 메세지를 이쁘게 해서 현재의 상황을 알려준다. > 던지면 안된다.
        if (member.isPresent()){
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setMemberId(member.get().getId());
            loginResponse.setMemberName(member.get().getName());

            SingleResult<LoginResponse> result = new SingleResult<>();
            result.setData(loginResponse);
            result.setCode(0);
            result.setMsg("로그인 성공");

            return result;
        }else {//회원데이터가 없으면
            SingleResult<LoginResponse> result = new SingleResult<>();
            result.setCode(-1);
            result.setMsg("아이디나 비밀번호를 다시 확인해 주세요");

            return result;
        }

    }
}
