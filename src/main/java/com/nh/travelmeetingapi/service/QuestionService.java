package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.Question;
import com.nh.travelmeetingapi.model.Question.QuestionChangeRequest;
import com.nh.travelmeetingapi.model.Question.QuestionItem;
import com.nh.travelmeetingapi.model.Question.QuestionRequest;
import com.nh.travelmeetingapi.model.Question.QuestionResponse;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class QuestionService {
    private final QuestionRepository questionRepository;

    public Question getData(long questionId){
        return questionRepository.findById(questionId).orElseThrow();
    }

    public void setQuestion(Member member, QuestionRequest request){
        Question addData = new Question();
        addData.setMember(member);
        addData.setTitle(request.getTitle());
        addData.setContent(request.getContent());
        addData.setInputDay(LocalDateTime.now());

        questionRepository.save(addData);
    }
    public List<QuestionItem> getQuestions(){
        List<Question> originData = questionRepository.findAll();

        List<QuestionItem> result = new LinkedList<>();
        for (Question question : originData){
            QuestionItem addList = new QuestionItem();
            addList.setId(question.getId());
            addList.setMemberName(question.getMember().getName());
            addList.setTitle(question.getTitle());
            addList.setContent(question.getContent());
            addList.setInputDay(question.getInputDay().toString());

            result.add(addList);
        }
        return result;
    }
    public QuestionResponse getQuestion(long id){
    Question originData = questionRepository.findById(id).orElseThrow();

    QuestionResponse response = new QuestionResponse();
    response.setId(originData.getId());
    response.setMemberName(originData.getMember().getName());
    response.setTitle(originData.getTitle());
    response.setContent(originData.getContent());
    response.setInputDay(originData.getInputDay().toString());

    return response;
    }

    public void putQuestion(Long questionId, QuestionChangeRequest request){
        Question originData = questionRepository.findById(questionId).orElseThrow();
        originData.setTitle(request.getTitle());
        originData.setContent(request.getContent());

        questionRepository.save(originData);
    }

    public void delQuestion(long id){
        questionRepository.findById(id);
    }

    public ListResult<Question> getQuestion(int pageNum){
        PageRequest pageRequest = PageRequest.of(-1,5);
        Page<Question> questions = questionRepository.findAll(pageRequest);

        ListResult<Question> result = new ListResult<>();
        result.setList(questions.getContent());
        result.setTotalCount(questions.getTotalElements());
        result.setTotalPage(questions.getTotalPages());
        result.setCurrentPage(questions.getTotalPages()+1);

        return result;
    }

}
