package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Board;
import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.model.Package.PackageItem;
import com.nh.travelmeetingapi.model.Package.PackageRequest;
import com.nh.travelmeetingapi.model.Package.PackageResponse;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.repository.PackageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PackageIdService {
    private final PackageRepository packageRepository;

    public PackageId getData(Long id){return packageRepository.findById(id).orElseThrow();}
    public void setPackage(PackageRequest request){
        PackageId addData = new PackageId();
        addData.setPackageName(request.getPackageName());
        addData.setMatchingStartDay(request.getMatchingStartDay());
        addData.setMatchingEndDay(request.getMatchingEndDay());
        addData.setPlace(request.getPlace());

        packageRepository.save(addData);
    }

    public List<PackageItem> getPackages(){
        List<PackageId> originData = packageRepository.findAll();
        List<PackageItem> result = new LinkedList<>();

        for (PackageId packageId : originData){
            PackageItem addList = new PackageItem();
            addList.setId(packageId.getId());
            addList.setPackageName(packageId.getPackageName());
            addList.setMatchingStartDay(packageId.getMatchingStartDay().toString());
            addList.setMatchingEndDay(packageId.getMatchingEndDay().toString());
            addList.setPlace(packageId.getPlace());

            result.add(addList);
        }
        return result;
    }
    public PackageResponse getPackageOne(long id){
        PackageId originData = packageRepository.findById(id).orElseThrow();

        PackageResponse response = new PackageResponse();
        response.setId(originData.getId());
        response.setPackageName(originData.getPackageName());
        response.setMatchingStartDay(originData.getMatchingStartDay().toString());
        response.setMatchingEndDay(originData.getMatchingEndDay().toString());
        response.setPlace(originData.getPlace());

        return response;
    }

    public void delPackage(long id){
        packageRepository.deleteById(id);
    }

    public ListResult<PackageId> getPackagePage(int pageNum){
        PageRequest pageRequest = PageRequest.of(-1,9);
        Page<PackageId> packageIds = packageRepository.findAll(pageRequest);

        ListResult<PackageId> result = new ListResult<>();
        result.setList(packageIds.getContent());
        result.setTotalCount(packageIds.getTotalElements());
        result.setTotalPage(packageIds.getTotalPages());
        result.setCurrentPage(packageIds.getTotalPages()+1);

        return result;
    }
}
