package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Applicant;
import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.entity.MatchingConfirmationConsent;
import com.nh.travelmeetingapi.model.MatchingConfirmationConsent.MatchingCreateRequest;
import com.nh.travelmeetingapi.model.MatchingConfirmationConsent.MatchingItem;
import com.nh.travelmeetingapi.model.MatchingConfirmationConsent.MatchingResponse;
import com.nh.travelmeetingapi.repository.ApplicantRepository;
import com.nh.travelmeetingapi.repository.MatchingConfirmationConsentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MatchingConfirmationConsentService { //확정자 관리자 페이지 등록
    private final ApplicantRepository applicantRepository;
    private final MatchingConfirmationConsentRepository matchingConfirmationConsentRepository;

    public MatchingConfirmationConsent getData(long id) {
        return matchingConfirmationConsentRepository.findById(id).orElseThrow();

    }


    public void setMatching(PackageId packageId)  {
        List<Applicant> applicants = applicantRepository.findAllByIsConfirmation(true);
        // 신청자 리스트에서 IsConfirmation true인 데이터만 가지고 와라


        for (Applicant applicant : applicants) {
            List<MatchingConfirmationConsent> matchingConfirmationConsents = matchingConfirmationConsentRepository.findAllByPackageIdAndApplicant_Member_IsMen(packageId, true);
            // 패키지 아이디와 남자만 가지고 와라.
            List<MatchingConfirmationConsent> matchingConfirmationConsentsFalse = matchingConfirmationConsentRepository.findAllByPackageIdAndApplicant_Member_IsMen(packageId, false);
            // 패키지 아이디와 여자만 가지고 와라.
            if (applicant.getMember().getIsMen()) {
                // 신청자가 남자면은 true 여자이면 false 이다.
                if (matchingConfirmationConsents.size() >= 4) { // 패키지 아이디와 남자가 같은데 4명 이상이면 true 즉 3명 이하 일 경우 실행
                } else {
                    MatchingConfirmationConsent addData = new MatchingConfirmationConsent();
                    addData.setPackageId(packageId);
                    addData.setApplicant(applicant);

                    applicant.setIsConfirmation(false);
                    applicantRepository.save(applicant); // 가져온 맴버의 상태를 false로 변경 한다.

                    matchingConfirmationConsentRepository.save(addData); // 완성된 매칭을 entity에 저장.
                }

            } else {
                if (matchingConfirmationConsentsFalse.size() >= 4) {

                } else {
                    MatchingConfirmationConsent addData = new MatchingConfirmationConsent();
                    addData.setPackageId(packageId);
                    addData.setApplicant(applicant);


                    applicant.setIsConfirmation(false);
                    applicantRepository.save(applicant);

                    matchingConfirmationConsentRepository.save(addData);
                }
            }
        }

    }

    public List<MatchingItem> getMatchings(){
        List<MatchingConfirmationConsent> originList = matchingConfirmationConsentRepository.findAll();

        List<MatchingItem> result = new LinkedList<>();

        for (MatchingConfirmationConsent matchingConfirmationConsent : originList){
            MatchingItem addItem = new MatchingItem();
            addItem.setId(matchingConfirmationConsent.getId());
            addItem.setPackageId(matchingConfirmationConsent.getPackageId().getId());
            addItem.setApplicant(matchingConfirmationConsent.getApplicant().getId());



            result.add(addItem);
        }

        return result;
    }

    public MatchingResponse getMatching(long matchingId){
        MatchingConfirmationConsent originData = matchingConfirmationConsentRepository.findById(matchingId).orElseThrow();

        MatchingResponse response = new MatchingResponse();
        response.setId(originData.getId());
        response.setApplicant(originData.getApplicant().getId());


        return response;
    }

    public void delMatching(long matchingId){

        matchingConfirmationConsentRepository.deleteById(matchingId);

    }
}
