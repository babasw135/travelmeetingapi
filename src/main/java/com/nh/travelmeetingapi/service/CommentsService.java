package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Board;
import com.nh.travelmeetingapi.entity.Comments;
import com.nh.travelmeetingapi.model.Comments.CommentsItem;
import com.nh.travelmeetingapi.model.Comments.CommentsRequest;
import com.nh.travelmeetingapi.model.Comments.CommentsResponse;
import com.nh.travelmeetingapi.repository.CommentsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentsService {
    private final CommentsRepository commentsRepository;

    public void setComments(Board board,CommentsRequest request){
        Comments addData = new Comments();
        addData.setBoard(board);
        addData.setComments(request.getComments());
        addData.setWritingTime(LocalDateTime.now());

        commentsRepository.save(addData);
    }

    public List<CommentsItem> getComments(){
        List<Comments> originData = commentsRepository.findAll();

        List<CommentsItem> result = new LinkedList<>();
        for (Comments comments : originData){
            CommentsItem addList = new CommentsItem();
            addList.setId(comments.getId());
            addList.setBoard(comments.getId());
            addList.setComments(comments.getComments());
            addList.setWritingTime(comments.getWritingTime().toString());

            result.add(addList);
        }
        return result;
    }

    public CommentsResponse getComment(long id){
        Comments originList = commentsRepository.findById(id).orElseThrow();

        CommentsResponse response = new CommentsResponse();
        response.setId(originList.getId());
        response.setBoard(originList.getId());
        response.setComments(originList.getComments());
        response.setWritingTime(originList.getWritingTime().toString());

        return response;
    }

    public void putComment(long id, CommentsRequest response){ // 댓글 수정
        Comments originData = commentsRepository.findById(id).orElseThrow();
        originData.setComments(response.getComments());
        originData.setWritingTime(LocalDateTime.now());

        commentsRepository.save(originData);
    }

    public void delComment(long id){
        commentsRepository.deleteById(id);
    }
}
