package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Question;
import com.nh.travelmeetingapi.entity.QuestionAnswer;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerChangeRequest;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerCreateRequest;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerItem;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerResponse;
import com.nh.travelmeetingapi.repository.QuestionAnswerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class QuestionAnswerService {
    private final QuestionAnswerRepository questionAnswerRepository;

    // 답변 등록
    public void setQuestionAnswer(Question question, QuestionAnswerCreateRequest request){
        QuestionAnswer addData = new QuestionAnswer();
        addData.setQuestion(question);
        addData.setQuestionAnswer(request.getQuestionAnswer());

        questionAnswerRepository.save(addData);
    }

    // 답변 전체 보기
    public List<QuestionAnswerItem> getQuestionAnswers(){
        List<QuestionAnswer> originList = questionAnswerRepository.findAll();

        List<QuestionAnswerItem> result = new LinkedList<>();

        for (QuestionAnswer questionAnswer : originList){
            QuestionAnswerItem addItem = new QuestionAnswerItem();
            addItem.setQuestionAnswer(questionAnswer.getQuestionAnswer());

            result.add(addItem);
        }

        return result;
    }

    // 답변 한개 보기
    public QuestionAnswerResponse getQuestionAnswer(long questionAnswerId){
        QuestionAnswer originData = questionAnswerRepository.findById(questionAnswerId).orElseThrow();

        QuestionAnswerResponse response = new QuestionAnswerResponse();
        response.setQuestionAnswer(originData.getQuestionAnswer());

        return response;
    }

    // 답변 수정
    public void putQuestionAnswer(long questionAnswerId, QuestionAnswerChangeRequest request){
        QuestionAnswer originData = questionAnswerRepository.findById(questionAnswerId).orElseThrow();
        originData.setQuestionAnswer(request.getQuestionAnswer());

        questionAnswerRepository.save(originData);
    }

    // 답변 삭제
    public void delQuestionAnswer(long questionAnswerId){
        questionAnswerRepository.deleteById(questionAnswerId);
    }
}
