package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Board;
import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.model.board.BoardCreateRequest;
import com.nh.travelmeetingapi.model.board.BoardItem;
import com.nh.travelmeetingapi.model.board.BoardResponse;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public Board getData(long id){return boardRepository.findById(id).orElseThrow();}

    public void setBoard(Member member, BoardCreateRequest request) {
        Board addData = new Board();
        addData.setMember(member);
        addData.setImg(request.getImg());
        addData.setDateWrite(LocalDateTime.now());
        addData.setTitle(request.getTitle());
        addData.setContent(request.getContent());

        boardRepository.save(addData);
    }

    public List<BoardItem> getBoards() {
        List<Board> originList = boardRepository.findAll();

        List<BoardItem> result = new LinkedList<>();

        for (Board board : originList){
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberName(board.getMember().getName());
            addItem.setImg(board.getImg());
            addItem.setDateWrite(board.getDateWrite());
            addItem.setTitle(board.getTitle());
            addItem.setContent(board.getContent());

            result.add(addItem);
        }

        return result;
    }

    public BoardResponse getBoard(long boardId) {
     Board originData = boardRepository.findById(boardId).orElseThrow();

     BoardResponse response = new BoardResponse();
     response.setId(originData.getId());
     response.setImg(originData.getImg());
     response.setMemberName(originData.getMember().getName());
     response.setDateWrite(originData.getDateWrite());
     response.setTitle(originData.getTitle());
     response.setContent(originData.getContent());

     return response;
    }

    public void delBoard(long boardId){
        boardRepository.deleteById(boardId);
    }

    public ListResult<Board> getBoardPage(int pageNum){
        PageRequest pageRequest = PageRequest.of(-1,9);
        Page<Board> boards = boardRepository.findAll(pageRequest);

        ListResult<Board> result = new ListResult<>();
        result.setList(boards.getContent());
        result.setTotalCount(boards.getTotalElements());
        result.setTotalPage(boards.getTotalPages());
        result.setCurrentPage(boards.getTotalPages()+1);

        return result;
    }
}
