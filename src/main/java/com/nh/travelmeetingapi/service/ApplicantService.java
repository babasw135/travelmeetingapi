package com.nh.travelmeetingapi.service;


import com.nh.travelmeetingapi.entity.Applicant;
import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.model.applicant.ApplicantItem;
import com.nh.travelmeetingapi.model.applicant.ApplicantRequest;
import com.nh.travelmeetingapi.model.applicant.ApplicantResponse;
import com.nh.travelmeetingapi.repository.ApplicantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ApplicantService {
    private final ApplicantRepository applicantRepository;

    public Applicant getData(Long id){return applicantRepository.findById(id).orElseThrow();}
    public void setApplicant(Member member, ApplicantRequest request) {

        Applicant addData = new Applicant();
        addData.setMember(member);
        addData.setIsConfirmation(true);
        addData.setApplicantDay(LocalDateTime.now());

        applicantRepository.save(addData);
    }

    public List<ApplicantItem> getApps(){
        List<Applicant> originData = applicantRepository.findAll();

        List<ApplicantItem> result = new LinkedList<>();
        for (Applicant applicant1 : originData){
            ApplicantItem addList = new ApplicantItem();
            addList.setId(applicant1.getId());
            addList.setMember(applicant1.getId());
            addList.setMemberName(applicant1.getMember().getName());
            addList.setIsConfirmation(applicant1.getIsConfirmation().toString());
            addList.setApplicantDay(applicant1.getApplicantDay().toString());

            result.add(addList);
        }
        return result;
    }
    public ApplicantResponse getApp(long id){
        Applicant originData = applicantRepository.findById(id).orElseThrow();

        ApplicantResponse response = new ApplicantResponse();
        response.setId(originData.getId());
        response.setMember(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setIsConfirmation(originData.getIsConfirmation().toString());
        response.setApplicantDay(originData.getApplicantDay().toString());

        return response;
    }

    public void delApp(long id){
        applicantRepository.deleteById(id);
    }

}
